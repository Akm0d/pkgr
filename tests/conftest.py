from dict_tools import data
import pytest

@pytest.fixture(scope="function")
def hub(hub):
  """
    provides a full hub that is used as a reference for mock_hub
  """
  hub.pop.sub.add(dyne_name="pkgr")
  yield hub

@pytest.fixture(scope="function")
def mock_hub(hub, tmpdir):
    mock_hub = hub.pop.testing.mock_hub()
    mock_hub.OPT = data.NamespaceDict()
    mock_hub.pkgr.proj_name = "salt"
    mock_hub.pkgr.VER = "3000.1"
    mock_hub.pkgr.SDIR = tmpdir.mkdir("sdir")
    mock_hub.pkgr.CDIR = tmpdir.mkdir("cdir")
    mock_hub.pkgr.BDIR = tmpdir.mkdir("bdir")
    mock_hub.pkgr.SPEC = "{ version }"
    yield mock_hub
