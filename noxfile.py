import os
import nox

def _install_requirements(session):
    """
    install python requirements
    """
    # install pkgr dependencies
    req = os.path.join("requirements.txt")
    session.install("-r", req)

    # install pkgr test dependencies
    req = os.path.join("tests", "requirements.txt")
    session.install("-r", req)

@nox.session
def test_rpm(session):
    _install_requirements(session)

    tests = ["tests/all", "tests/rpm"]
    if session.posargs:
        tests = session.posargs
    session.run("pytest", "-vv", *tests)

@nox.session
def test_deb(session):
    _install_requirements(session)

    tests = ["tests/all", "tests/deb"]
    if session.posargs:
        tests = session.posargs
    session.run("pytest", "-vv", *tests)
