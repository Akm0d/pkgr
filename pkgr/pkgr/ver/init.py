def gather(hub):
    """
    Gather the version number from the underlying project
    """
    ver = hub.OPT.pkgr.ver
    hub.pkgr.VER = ver
    if ver in hub.pkgr.ver:
        hub.pkgr.VER = getattr(hub, f"pkgr.ver.{ver}.gather")()
        return
