# Import python libs
import os
import shutil
import subprocess


def gather(hub):
    """
    Gather the version number from salt
    """
    pybin = hub.OPT.pkgr.python
    cdir = os.path.join(hub.pkgr.SDIR, "salt")
    cwd = os.getcwd()
    os.chdir(cdir)
    subprocess.run(f"{pybin} setup.py sdist", shell=True)
    for fn in os.listdir("dist"):
        version = fn[fn.index("-") + 1 : fn[: fn.rindex(".")].rindex(".")].replace("-", "_")
        os.rename(os.path.join("dist", fn),
                  os.path.join("dist", "salt-" + version.replace("-", "_") + ".tar.gz"))
    os.chdir(cwd)
    return version
