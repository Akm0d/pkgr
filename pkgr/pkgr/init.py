# Prepare primary sources as tarball
# Prep temp rpm build tree
# Run the spec file through jinja
# Save output into rpm build tree
# Copy sources into rpm build tree
# build rpm

# Import python libs
import subprocess
import tempfile
import os


def __init__(hub):
    hub.pop.sub.load_subdirs(hub.pkgr)
    hub.pkgr.BDIR = tempfile.mkdtemp()
    hub.pkgr.SDIR = tempfile.mkdtemp()
    hub.pkgr.CDIR = os.getcwd()


def cli(hub):
    hub.pop.config.load(["pkgr"], cli="pkgr")
    hub.pkgr.init.gather()
    getattr(hub, f"pkgr.{hub.OPT.pkgr.system}.render", lambda: 0)()
    getattr(hub, f"pkgr.{hub.OPT.pkgr.system}.build")()


def gather(hub):
    os.chdir(hub.pkgr.SDIR)
    git = hub.OPT.pkgr.git
    hub.pkgr.proj_name = git.rsplit("/", 1)[-1].split(".")[0]
    if not hub.OPT.pkgr.ref:
        # We need the full depth to get the correct version number
        cmd = f"git clone {git}"
        subprocess.run(cmd, shell=True)
    else:
        cmd = f"git clone {git}"
        subprocess.run(cmd, shell=True)
        tdir = os.path.basename(git)
        if tdir.endswith(".git"):
            tdir = tdir[:-4]
        os.chdir(tdir)
        cmd = f"git checkout {hub.OPT.pkgr.ref}"
        subprocess.run(cmd, shell=True)
    os.chdir(hub.pkgr.CDIR)
    hub.pkgr.ver.init.gather()
