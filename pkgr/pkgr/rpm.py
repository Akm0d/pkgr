# Import python libs
import subprocess
import os
import shutil
import sys

# Import third party libs
import jinja2


def __virtual__(hub):
    return all(shutil.which(cmd) for cmd in ("tar", "rpmbuild"))


def build(hub):
    """
    Build the package!
    """
    # Prep the build tree
    # Copy the sources in
    # place the spec file down
    # run rpmbuild -ba
    proj = os.path.join(hub.pkgr.SDIR, hub.pkgr.proj_name, "dist", f"{hub.pkgr.proj_name}-{hub.pkgr.VER}.tar.gz")
    os.chdir(hub.pkgr.CDIR)

    for dir_ in ("SOURCES", "SPECS", "BUILD", "SRPMS"):
        tdir = os.path.join(hub.pkgr.BDIR, dir_)
        os.makedirs(tdir)
    spec = os.path.join(hub.pkgr.BDIR, "SPECS", "pkg.spec")
    sources = os.path.join(hub.pkgr.BDIR, "SOURCES")
    rpms = os.path.join(hub.pkgr.BDIR, "RPMS")
    srpms = os.path.join(hub.pkgr.BDIR, "SRPMS")
    shutil.copy(proj, sources)
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)
    with open(spec, "w+") as wfh:
        wfh.write(hub.pkgr.SPEC)
    cmd = f"rpmbuild --define '_topdir {hub.pkgr.BDIR}' -ba {spec}"
    print(f"[pkgr] Running: {cmd}")
    sys.stdout.flush()
    retcode = subprocess.run(cmd, shell=True).returncode
    if retcode != 0:
        print(f"[pkgr] {cmd} exited with return code: {retcode}")
        sys.exit(retcode)
    artifacts = os.path.join(hub.pkgr.CDIR, "artifacts")
    if not os.path.isdir(artifacts):
        print(f"[pkgr] Making Directory: {artifacts}")
        os.mkdir(artifacts)
    for root, dirs, files in os.walk(rpms):
        for fn in files:
            if fn.endswith("rpm"):
                full = os.path.join(root, fn)
                shutil.copy(full, artifacts)
    for root, dirs, files in os.walk(srpms):
        for fn in files:
            if fn.endswith("rpm"):
                full = os.path.join(root, fn)
                shutil.copy(full, artifacts)
    sys.stdout.flush()


def render(hub):
    """
    Render the spec file
    """
    opts = dict(hub.OPT.pkgr)
    opts["version"] = hub.pkgr.VER
    with open(hub.OPT.pkgr.spec) as rfh:
        data = rfh.read()
    template = jinja2.Template(data)
    hub.pkgr.SPEC = template.render(**opts)
